<?php

namespace Drupal\media_cleanup\Form;

/**
 * This image deletion from a node will be in three phases.
 *
 * 1. This file will be deleted from the database table entries, means from
 *    file_usage & file_managed.
 *
 * 2. Next file will be deleted from the field as well, this batch process will
 *    search for the image field and from there the row will be deleted.
 *
 * 3. Deleting file from the files folder is the third number of process.
 */

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure media_cleanup settings for this site.
 *
 * This class is the form which will fetch the nodes to delete and through a
 * batch process it will delete the images of those nodes.
 *
 * @extends \Drupal\Core\Form\ConfigFormBase.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager will be used to query the database.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Database connection will be used for custom queries in the direct tables.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, Connection $database, FileSystemInterface $fileSystem, EntityTypeBundleInfoInterface $entityTypeBundleInfo, EntityFieldManagerInterface $entityFieldManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->database = $database;
    $this->fileSystem = $fileSystem;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('file_system'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_cleanup_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['media_cleanup.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Getting the configuration of the form.
    $config = $this->config('media_cleanup.settings');

    // Get the entity type for which you want to list bundles.
    $entity_type_id = 'node';

    // Get the available bundles for the entity type.
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);

    // Create an options array for the select field.
    $bundle_options = [];
    foreach ($bundles as $bundle_id => $bundle_info) {
      $bundle_options[$bundle_id] = $bundle_info['label'] . ' (' . $bundle_id . ')';
    }

    $form['type'] = [
      '#title' => $this->t('Select'),
      '#type' => 'select',
      '#default_value' => $config->get('type'),
      '#description' => $this->t("Select the content type."),
      '#options' => $bundle_options,
      '#required' => TRUE,
    ];

    // Checks if the node is published or unpublished.
    $form['status'] = [
      '#title' => $this->t('Select Status'),
      '#type' => 'select',
      '#default_value' => $config->get('status'),
      '#description' => $this->t("Select the content type."),
      '#options' => [
        '' => $this->t('--Any--'),
        '0' => $this->t('Unpublished'),
        '1' => $this->t('Published'),
      ],
      '#required' => TRUE,
    ];

    // This field will fetch the created dates of the nodes.
    $form['date_field'] = [
      '#type' => 'textarea',
      '#title' => $this->t('List the Date'),
      '#default_value' => $config->get('date_field'),
      '#description' => $this->t('Enter a date and time in the format: <code>mm/dd/yyyy - hh:mm</code>. For example: 02/10/2017 - 06:07, Each in a new line'),
      '#required' => TRUE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Define the regex pattern to match the date and time format.
    $pattern = '/(\d{2}\/\d{2}\/\d{4} - \d{2}:\d{2})/';

    // Use preg_match_all to find all matching patterns in the input.
    if (!preg_match_all($pattern, $form_state->getValue('date_field'), $matches)) {
      $form_state->setErrorByName('date_field', $this->t('The format is not correct.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the form configuration.
    $this->config('media_cleanup.settings')
      ->set('date_field', $form_state->getValue('date_field'))
      ->set('type', $form_state->getValue('type'))
      ->set('status', $form_state->getValue('status'))
      ->save();
    // Define the regex pattern to match the date and time format.
    $pattern = '/(\d{2}\/\d{2}\/\d{4} - \d{2}:\d{2})/';

    // Initialize an empty array for timestamps.
    $timestamps = [];

    // Check if the date field is not empty and matches the pattern.
    if (preg_match_all($pattern, $form_state->getValue('date_field'), $matches)) {
      $timestamps = $matches[0];
    }

    // Define the batch process configuration.
    $batch = [
      'title' => $this->t('Deleting Unpublished Files'),
      'operations' => [],
      'init_message' => $this->t('Starting file deletion process...'),
      'progress_message' => $this->t('Processed @current out of @total files.'),
      'error_message' => $this->t('An error occurred during file deletion.'),
      'finished' => 'media_cleanup_batch_finished',
    ];

    foreach ($timestamps as $timestamp_str) {
      // Convert the timestamp string to a Unix timestamp.
      $timestamp = \DateTime::createFromFormat('m/d/Y - H:i', $timestamp_str);

      // Define the start and end of the one-minute range for this timestamp.
      $start_range = $timestamp->getTimestamp();
      $end_range = $start_range + 59;

      // Define the query to fetch unpublished nodes.
      $query = $this->entityTypeManager->getStorage('node')
        ->getQuery()
        ->condition('type', $form_state->getValue('type'))
        // Unpublished nodes have a status of 0.
        ->condition('status', $form_state->getValue('status'))
        ->accessCheck(TRUE)
        ->condition('created', [$start_range, $end_range], 'BETWEEN');

      $nids = $query->execute();
      // Iterate through the node IDs and retrieve associated fids from the
      // file_usage table.
      foreach ($nids as $nid) {
        // Query the file_usage table to get fids.
        $fid_query = $this->database->select('file_usage', 'fu')
          ->fields('fu', ['fid'])
          ->condition('fu.id', $nid)
          ->execute();

        // Reset $file_ids for the next iteration.
        $file_ids = [];

        while ($row = $fid_query->fetchAssoc()) {
          $file_ids[] = $row['fid'];
        }

        // Check if there are any fids associated with this nid.
        if (!empty($file_ids)) {
          foreach ($file_ids as $file_id) {
            // Load the file entity to get the file path.
            $file_entity = $this->entityTypeManager->getStorage('file')->load($file_id);
            $batch['operations'][] = [
              'media_cleanup_batch_process_node',
                [$file_entity, $form_state->getValue('type'), $nid, $file_id],
            ];
          }
        }
      }
    }

    // Set the batch process.
    batch_set($batch);
  }

}
